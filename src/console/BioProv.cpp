#include "BioProv.hh"

////////////////////////////////////////////////////////////////////////////////
// Helper

namespace
{
std::string time_and_date()
{
  auto now = std::chrono::system_clock::now();
  auto in_time_t = std::chrono::system_clock::to_time_t(now);

  std::stringstream ss;
  ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
  return ss.str();
}
}

////////////////////////////////////////////////////////////////////////////////

BioProv::BioProv(const std::string& bioprovEndpoint, const std::string& serviceContext) :
  _bioprovEndpoint(bioprovEndpoint),
  _serviceContext(serviceContext)
  // _serviceContextLocal(serviceContext)
{
//   _serviceContext = R"(
//     {
//         "bioprov": "http://bioprov.poli.usp.br/",
//         "dc": "http://purl.org/dc/elements/1.1/",
//         "dcterms": "http://purl.org/dc/terms/",
//         "dcmitype" : "http://purl.org/dc/dcmitype/",
//         "prov": "http://www.w3.org/ns/prov#",
//         "dwc": "http://rs.tdwg.org/dwc/terms/",
//         "dcat": "http://www.w3.org/ns/dcat#",
//         "foaf": "http://xmlns.com/foaf/0.1/",
//         "xsd": "http://www.w3.org/2001/XMLSchema#",
//         "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
//         "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
//         "time": "http://www.w3.org/2006/time#",
//         "owl": "http://www.w3.org/2002/07/owl#",
//         "title": "dcterms:title",
//         "subject": "dcterms:subject",
//         "description": "dcterms:description",
//         "creator": "dcterms:creator",
//         "created": "dcterms:created",
//         "modified": "dcterms:modified",
//         "contributor": "dcterms:contributor",
//         "source": "dcterms:source",
//         "type": "dcterms:type",
//         "hasVersion": "dcterms:hasVersion",
//         "value": "rdf:value",
//         "agent": { "@type": "@id", "@id": "prov:agent" },
//         "entity": { "@type": "@id", "@id": "prov:entity" },
//         "activity": { "@type": "@id", "@id": "prov:activity" },
//         "hadPlan": { "@type": "@id", "@id": "prov:hadPlan" },
//         "hadRole": { "@type": "@id", "@id": "prov:hadRole" },
//         "atLocation": {"@type":"@id", "@id": "prov:atLocation"},
//         "wasAttributedTo": { "@type": "@id", "@id": "prov:wasAttributedTo" },
//         "wasAssociatedWith": { "@type": "@id", "@id": "prov:wasAssociatedWith" },
//         "association": { "@type": "@id", "@id": "prov:qualifiedAssociation" },
//         "used": { "@type": "@id", "@id": "prov:used" },
//         "usage": { "@type": "@id", "@id": "prov:qualifiedUsage" },
//         "wasGeneratedBy": { "@type": "@id", "@id": "prov:wasGeneratedBy" },
//         "generated": { "@reverse": "prov:wasGeneratedBy", "@type": "@id" },
//         "generation": { "@type": "@id", "@id": "prov:qualifiedGeneration" },
//         "wasInformedBy": { "@type":"@id", "@id":"prov:wasInformedBy" },
//         "wasDerivedFrom": { "@type": "@id", "@id": "prov:wasDerivedFrom" },
//         "wasRevisionOf": { "@type": "@id", "@id": "prov:wasRevisionOf" },
//         "wasQuotedFrom": { "@type": "@id", "@id": "prov:wasQuotedFrom" },
//         "specializationOf": { "@type": "@id", "@id": "prov:specializationOf" },
//         "alternateOf": { "@type": "@id", "@id": "prov:alternateOf" },
//         "startedAtTime": "prov:startedAtTime",
//         "endedAtTime": "prov:endedAtTime",
//         "invalidatedAtTime": "prov:invalidatedAtTime",
//         "generatedAtTime": "prov:generatedAtTime",
//         "actedOnBehalfOf": { "@type":"@id", "@id":"prov:actedOnBehalfOf"},
//         "hadMember": { "@type":"@id", "@id":"prov:hadMember"},
//         "name" : "foaf:name",
//         "givenName" : "foaf:givenName",
//         "familyName" : "foaf:familyName",
//         "mbox" : { "@type": "@id", "@id": "foaf:mbox" },
//         "homepage" : { "@type": "@id", "@id": "foaf:homepage" },
//         "comment" : "rdfs:comment",
//         "seeAlso" : { "@type": "@id", "@id": "rdfs:seeAlso"},
//         "label" : "rdfs:label",
//         "id" : {"@type": "@id", "@id": "@id"},
//         "sameAs" : { "@type":"@id", "@id":"owl:sameAs" },
//         "a" : { "@type":"@id", "@id":"rdf:type" },
//         "dataType" : "bioprov:dataType",
//         "activityType" : "bioprov:activityType",
//         "Collection" : { "@type":"@id", "@id":"dcmitype:Collection" },
//         "Dataset" : { "@type":"@id", "@id":"dcmitype:Dataset" },
//         "Event" : { "@type":"@id", "@id":"dcmitype:Event" },
//         "Image" : { "@type":"@id", "@id":"dcmitype:Image" },
//         "InteractiveResource" : { "@type":"@id", "@id":"dcmitype:InteractiveResource" },
//         "MovingImage" : { "@type":"@id", "@id":"dcmitype:MovingImage" },
//         "PhysicalObject" : { "@type":"@id", "@id":"dcmitype:PhysicalObject" },
//         "Service" : { "@type":"@id", "@id":"dcmitype:Service" },
//         "Software" : { "@type":"@id", "@id":"dcmitype:Software" },
//         "Sound" : { "@type":"@id", "@id":"dcmitype:Sound" },
//         "StillImage" : { "@type":"@id", "@id":"dcmitype:StillImage" },
//         "Text" : { "@type":"@id", "@id":"dcmitype:Text" },
//         "scientificName" : "dwc:scientificName",
//         "temporal" : { "@type":"@id", "@id":"dcterms:temporal" },
//         "date" : "dcterms:date",
//         "spatial" : "dcterms:spatial",
//         "coverage" : { "@type":"@id", "@id":"dc:coverage" },
//         "identifier" : "dcterms:identifier",
//         "cause": "bioprov:cause",
//         "problemDelimitation": "bioprov:problemDelimitation",
//         "hypothesis": "bioprov:hypothesis",
//         "goals": "bioprov:goals",
//         "method" : "bioprov:method",
//         "has_requirements": "bioprov:has_requirements",
//         "taxonomicCoverage": "bioprov:taxonomicCoverage",
//         "has_results": "bioprov:has_results",
//         "studyAreaDescription": "bioprov:studyAreaDescription",
//         "funding": "bioprov:funding",
//         "activityType": "bioprov:activityType",
//         "decisions": "bioprov:decisions",
//         "assumptions": "bioprov:assumptions",
//         "computationalArchitectureUsed": "bioprov:computationalArchitectureUsed",
//         "FunctionalDependencies": "bioprov:FunctionalDependencies",
//         "entityFormat": "bioprov:entityFormat",
//         "entityType": "bioprov:entityType",
//         "resourceType": "bioprov:resourceType",
//         "numberOfRecords": "bioprov:numberOfRecords",
//         "source": { "@type":"@id", "@id":"dcterms:source" },
//         "created": "dcterms:created",
//         "isVersionOf": "dcterms:isVersionOf",
//         "hasDuration": "time:hasDuration"

//     }
// )"_json;

}

////////////////////////////////////////////////////////////////////////////////

std::string BioProv::createExperiment(json& j)
{
  // std::string service = "experiments/";
  std::string service = "bundles/";
  j["@context"] = _serviceContext;
  if (j["created"] == nullptr)
    j["created"] = time_and_date();
  auto responseData = postData(service, {}, j);
  return json::parse(responseData)["@id"];
}

json BioProv::getExperiments()
{
  // std::string service = "experiments/";
  std::string service = "bundles/";
  auto responseData = getData(service, {});
  return json::parse(responseData)["@graph"];
}

json BioProv::getExperimentById(const std::string& id)
{
  // std::string service = "experiments/" + cpr::util::urlEncode(id);
  std::string service = "bundles/" + cpr::util::urlEncode(id);
  auto responseData = getData(service, {});
  return json::parse(responseData);
}

////////////////////////////////////////////////////////////////////////////////

std::string BioProv::createEntityProvenance(const std::string& experiment, json& j)
{
  std::string service = "entities/";
  j["@context"] = _serviceContext;
  if (j["created"] == nullptr)
    j["created"] = time_and_date();
  // auto responseData = postData(service, {{"experiment", experiment}}, j);
  auto responseData = postData(service, {{"bundle", experiment}}, j);
  return json::parse(responseData)["@id"];
}

json BioProv::getEntities()
{
  std::string service = "entities/";
  auto responseData = getData(service, {});
  return json::parse(responseData)["@graph"];
}

json BioProv::getEntityById(const std::string& id)
{
  std::string service = "entities/" + cpr::util::urlEncode(id);
  auto responseData = getData(service, {});
  return json::parse(responseData);
}

////////////////////////////////////////////////////////////////////////////////

std::string BioProv::createActivityProvenance(const std::string& experiment, json& j)
{
  std::string service = "activities/";
  j["@context"] = _serviceContext;
  if (j["created"] == nullptr)
    j["created"] = time_and_date();
  // auto responseData = postData(service, {{"experiment", experiment}}, j);
  auto responseData = postData(service, {{"bundle", experiment}}, j);
  return json::parse(responseData)["@id"];
}

json BioProv::getActivities()
{
  std::string service = "activities/";
  auto responseData = getData(service, {});
  return json::parse(responseData)["@graph"];
}

json BioProv::getActivityById(const std::string& id)
{
  std::string service = "activities/" + cpr::util::urlEncode(id);
  auto responseData = getData(service, {});
  return json::parse(responseData);
}

////////////////////////////////////////////////////////////////////////////////

std::string BioProv::createAgentProvenance(const std::string& experiment, json& j)
{
  std::string service = "agents/";
  j["@context"] = _serviceContext;
  if (j["created"] == nullptr)
    j["created"] = time_and_date();
  // auto responseData = postData(service, {{"experiment", experiment}}, j);
  auto responseData = postData(service, {{"bundle", experiment}}, j);
  return json::parse(responseData)["@id"];
}

json BioProv::getAgents()
{
  std::string service = "agents/";
  auto responseData = getData(service, {});
  return json::parse(responseData)["@graph"];
}

json BioProv::getAgentById(const std::string& id)
{
  std::string service = "agents/" + cpr::util::urlEncode(id);
  auto responseData = getData(service, {});
  return json::parse(responseData);
}

////////////////////////////////////////////////////////////////////////////////

json BioProv::getProvenanceOfExperiment(const std::string& id, const std::string& format)
{
  // std::string service = "experiments/" + cpr::util::urlEncode(id) + "/data";
  std::string service = "provenance/" + id;
  std::string destination = "provenance_experiment_" + format + "_" + time_and_date();
  if (format == "json")
  {
    auto responseData = getData(service, {{"format", format}});
    return json::parse(responseData)["@graph"];
  }
  else if (format == "report")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".provn");
    file << responseData;
    return {};
  }
  else if (format == "png")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".png");
    file << responseData;
    return {};
  }
  else if (format == "svg")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".svg");
    file << responseData;
    return {};
  }
  else if (format == "rdf")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".tll");
    file << responseData;
    return {};
  }
  else return {"invalid format"};
}

json BioProv::getProvenanceOfEntity(const std::string& id, const std::string& format)
{
  std::string service = "provenance/" + id;
  std::string destination = "provenance_entity_" + format + "_" + time_and_date();
  if (format == "json")
  {
      auto responseData = getData(service, {{"format", format}});
      return json::parse(responseData)["@graph"];
  }
  else if (format == "report")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".provn");
    file << responseData;
    return {};
  }
  else if (format == "png")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".png");
    file << responseData;
    return {};
  }
  else if (format == "svg")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".svg");
    file << responseData;
    return {};
  }
  else if (format == "rdf")
  {
    auto responseData = getData(service, {{"format", format}});
    std::ofstream file(destination + ".tll");
    file << responseData;
    return {};
  }
  else return {"invalid format"};
}

////////////////////////////////////////////////////////////////////////////////

std::string BioProv::postData(const std::string& service, const cpr::Parameters& params, const json& body)
{
  auto response = cpr::Post(cpr::Url{_bioprovEndpoint + service},
                           params,
                           cpr::Header{{"content-type", "application/json"}},
                           cpr::Body{body.dump()});
  if (response.status_code >= 400)
    throw std::runtime_error("Service Error. Status Code: " + response.status_code);
  return response.text;
}

std::string BioProv::getData(const std::string& service, const cpr::Parameters& params)
{
  auto response = cpr::Get(cpr::Url{_bioprovEndpoint + service},
                           params);
  if (response.status_code >= 400)
    throw std::runtime_error("Service Error. Status Code: " + response.status_code);
  return response.text;
}

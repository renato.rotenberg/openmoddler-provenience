#ifndef _PROVHELPERHH_
#define _PROVHELPERHH_

#include <string>  // string
#include <openmodeller/om.hh>
#include <json.hpp>

#include "BioProv.hh"

using json = nlohmann::json;

class ProvHelper
{
public:

  ProvHelper(const char* request_file);

  BioProv& getProv() { return _prov; }
  void createExperimentProvenance();
  void createRequestFileProvenance();
  void createOMProvenance(AlgParameter *result, AlgMetadata const *metadata);
  void createModellingProvenance();

private:

  BioProv _prov;
  FileParser _fp;
  std::string _idPrefix;
  std::string _experimentId;
  std::string _omId;
  std::vector<std::string> _inputParameters;
  std::vector<std::string> _fileInput;

};

#endif

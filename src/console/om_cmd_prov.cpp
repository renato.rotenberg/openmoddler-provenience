#include "om_cmd_prov.hh"

ProvHelper::ProvHelper(const char* request_file) :
  _prov(),
  _fp(std::string(request_file)),
  _idPrefix(),
  _experimentId(),
  _inputParameters()
{
  _prov         = BioProv(_fp.get("Provenance endpoint"),
                          _fp.get("Provenance context"));
  _idPrefix     = _fp.get("Id prefix");
  _experimentId = _idPrefix + _fp.get("Experiment Id");
}

void ProvHelper::createExperimentProvenance()
{
  json experiment = {{"@id", _experimentId}};
  _experimentId = _prov.createExperiment(experiment);
}

void ProvHelper::createRequestFileProvenance()
{
  json occurencesSources = {
    {"@id", _idPrefix + _fp.get("Occurrences source")},
    {"entityType", "Dataset"},
    {"description", "Source of the input points."},
    {"title", "Occurrence file of" + _fp.get("Occurrences group")}
  };

  _fileInput.push_back(_prov.createEntityProvenance(_experimentId, occurencesSources));

  json WKTCoord = {
    {"@id", _idPrefix + "WKTcoord"},
    {"entityType", "InputParameter"},
    {"identifier", "wktcoord"},
    {"description", "Spatial reference system (SRS) of all input points in WKT format."},
    {"value", _fp.get("WKT coord system")}
  };

  _fileInput.push_back(_prov.createEntityProvenance(_experimentId, WKTCoord));

  json spatiallyUnique = {
    {"@id", _idPrefix + "SU"},
    {"entityType", "InputParameter"},
    {"identifier", "spatiallyunique"},
    {"value", _fp.get("Spatially unique")}
  };
  if (spatiallyUnique["value"] == "")
    spatiallyUnique["value"] = "false";

  _fileInput.push_back(_prov.createEntityProvenance(_experimentId, spatiallyUnique));

  json environmentallyUnique = {
    {"@id", _idPrefix + "EU"},
    {"entityType", "InputParameter"},
    {"identifier", "environmentallyunique"},
    {"value", _fp.get("Environmentally unique")}
  };
  if (environmentallyUnique["value"] == "")
    environmentallyUnique["value"] = "false";

  _fileInput.push_back(_prov.createEntityProvenance(_experimentId, environmentallyUnique));

  for (const auto& mapName : _fp.getAll("Map"))
  {
    json map = {
      {"@id", _idPrefix + mapName},
      {"entityType", "Map"},
      {"description", "Maps (layers) to be used as environmental variables to generate the model."},
      {"title", "Map " + mapName}
    };

    _fileInput.push_back(_prov.createEntityProvenance(_experimentId, map));
  }

  json mask = {
    {"@id", _idPrefix + _fp.get("Mask")},
    {"entityType", "Mask"},
    {"description", "Mask to delimit the region to be used to generate the model (filter the species occurrences/absences points)."},
    {"title", "Mask " + _fp.get("Mask")}
  };

  _fileInput.push_back(_prov.createEntityProvenance(_experimentId, mask));

  json step1 = {
    {"@id", _idPrefix + "step1_retrieving"},
    {"activityType", "Acquisition"},
    {"title", "Retrieving occurrence data"},
    {"used", json(_fileInput)}
  };

  _prov.createActivityProvenance(_experimentId, step1);
}

void ProvHelper::createOMProvenance(AlgParameter *result, AlgMetadata const *metadata)
{
  AlgParamMetadata *param = metadata->param;
  AlgParamMetadata *end   = param + metadata->nparam;

  std::vector<std::string> parametersId;

  for ( ; param < end; param++, result++ )
  {
    json parameter = {
      {"@id", _idPrefix + param->id},
      {"entityType", "InputParameter"},
      {"identifier", param->id},
      {"title", param->name},
      {"description", param->overview},
    };

    parametersId.push_back(_prov.createEntityProvenance(_experimentId, parameter));
  }

  json algorithmAuthor = {
    {"name", metadata->code_author},
    {"mbox", "mailto:" + metadata->contact}
  };

  std::string algorithmAuthorId = _prov.createAgentProvenance(_experimentId, algorithmAuthor);

  json algorithm = {
    {"@id", _idPrefix + metadata->id},
    {"entityType", "software"},
    {"identifier", metadata->id},
    {"title", metadata->name},
    {"description", metadata->overview},
    {"wasAttributedTo", algorithmAuthorId},
    {"used", json(parametersId)}
  };

  std::string algorithmId = _prov.createEntityProvenance(_experimentId, algorithm);

  json omProv = {
    {"@id", "http://openmodeller.sourceforge.net/"},
    {"entityType", "software"},
    {"identifier", "openModeller"},
    {"description", "openModeller provides a flexible, robust, cross-platform environment to carry out ecological niche modeling experiments."},
    {"source", "https://sourceforge.net/projects/openmodeller/files/openModeller/1.5.0/"},
    {"used", algorithmId}
  };

  _omId = _prov.createEntityProvenance(_experimentId, omProv);
}

void ProvHelper::createModellingProvenance()
{
  std::vector<std::string> output;

  json outputXml = {
    {"@id", _idPrefix + _fp.get("Output model")},
    {"entityType", "rasterLayer"},
    {"description", "Output model (serialized model)."},
    {"title", "Model " + _fp.get("Output model")}
  };

  output.push_back(_prov.createEntityProvenance(_experimentId, outputXml));

  json outputMap = {
    {"@id", _idPrefix + _fp.get("Output file")},
    {"entityType", "rasterLayer"},
    {"description", "Output model (projected map)."},
    {"title", "Model " + _fp.get("Output file")}
  };

  output.push_back(_prov.createEntityProvenance(_experimentId, outputMap));

  _fileInput.push_back(_omId);

  json modeling = {
    {"@id", _idPrefix + "step2_modeling"},
    {"activityType", "Analisys"},
    {"title", "Species distribution modeling"},
    {"used", json(_fileInput)},
    {"generated", json(output)}
  };

  _prov.createActivityProvenance(_experimentId, modeling);
}

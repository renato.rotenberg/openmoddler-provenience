#ifndef _BIOPROVHH_
#define _BIOPROVHH_

#include <stdexcept>
#include <fstream>
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string
#include <cpr/cpr.h>
#include <cpr/util.h>
#include <json.hpp>

using json = nlohmann::json;

class BioProv
{
public:

  BioProv(const std::string& bioprovEndpoint, const std::string& serviceContext);
  BioProv()                          = default;
  BioProv(const BioProv&)            = default;
  BioProv(BioProv&&)                 = default;
  BioProv& operator=(const BioProv&) = default;
  BioProv& operator=(BioProv&&)      = default;

  std::string createExperiment(json& j);
  json getExperiments();
  json getExperimentById(const std::string& id);
  std::string createEntityProvenance(const std::string& experiment, json& j);
  json getEntities();
  json getEntityById(const std::string& id);
  std::string createActivityProvenance(const std::string& experiment, json& j);
  json getActivities();
  json getActivityById(const std::string& id);
  std::string createAgentProvenance(const std::string& experiment, json& j);
  json getAgents();
  json getAgentById(const std::string& id);

  json getProvenanceOfExperiment(const std::string& id, const std::string& format);
  json getProvenanceOfEntity(const std::string& id, const std::string& format);

private:

  std::string postData(const std::string& service, const cpr::Parameters& params, const json& body);
  std::string getData(const std::string& service, const cpr::Parameters& params);

  std::string _bioprovEndpoint;
  std::string _serviceContext;
  // std::string _serviceContextLocal;
  // json _serviceContext;

};

#endif
